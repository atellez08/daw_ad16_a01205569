<html>
  <head>
    <title>Lab8</title>
    <meta charset="UTF-8"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  </head>
  <body>
      <ul class="collection">
          <li class="collection-item"><i class="material-icons">add</i><strong>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu     atención.</strong>
              <ul>
                    <li>La función phpinfo() despliega toda la informacion sobre la versión de PHP que se está utilizando, tal como opciones, extensiones, el sistema, los valores locales, etc</li>
                    <li>La primera cosa que me llamó la atención es que dice que mi sitema es Linux, que es por iCloud</li>
                    <li>La segunda cosa que me llamó la atención fue que dice que hizo el build el 28 de Julio de este año, cuando claramente es 22 de sep</li>
                    <li>La tercera cosa que me llamó la atención fue la cantidad de detalle y la buena presentación con la que cuenta esta descripción del sistema y de la versión.</li>
                </ul>
          </li>
          <li class="collection-item"><i class="material-icons">add</i><strong>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</strong>
              <ul>
                    <li>Para pasar a una versión de producción es porque la página ya será desplegada, es decir ya estará en el servidor para ser utilizada en línea, por lo que se requerirá
                    hacer los ajustes del url, entre otras.</li>
                </ul>
          </li>
          <li class="collection-item"><i class="material-icons">add</i></strong>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</strong>
              <ul>
                    <li>Se ejecuta en el servidor con la ayuda del lenguaje PHP que es facilmete interpretado, además de ser mundialmente usado para este
                    tipo de aplicaciones.</li>
                </ul>
          </li>
        </ul>
    <p>
      <?php
  
        $num = array(2,3,6,7,8,3,6,1);
        function promedio($arr){
        $sum=0;
        foreach($arr as $val) {
         $sum=$sum+$val;        
        }
         $avg = $sum/count($arr);  
         return $avg;
        }
        
        echo 'Promedio: '.promedio($num)."\n";
        
        function mediana($arr){

            sort($arr);
 
            $n = count($arr);
            $mid = ($n) / 2;
            echo 'Mediana: '.$arr[$mid];  
            
        }
        echo mediana($num);
        
        function display($arr){
           echo '<ul>';
          foreach($arr as $val){
          echo "<li> $val </li>";
        }
        echo '</ul>';
        }
      sort($num);  
      display($num);
      rsort($num);
      display($num);
          
        function cuadCub($numero){
            echo '<table class="bordered">';
            echo '<tr>';
              echo '<th>'."Numero".'</td>';
              echo '<th>'."Cuadrado".'</td>';
              echo '<th>'."Cubo".'</td>';
            echo '</tr>';
            for($i=0; $i<$numero; ++$i){
                $cuadrado=$i*$i;
                $cubo=$i*$i*$i;
                echo '<tr>';
                echo '<td>'.$i.'</td>';
                echo '<td>'.$cuadrado.'</td>';
                echo '<td>'.$cubo.'</td>';
                echo '</tr>';
            }
            
            echo '</table>';
        }
       
        cuadCub(11);
 
      ?>
        
    </p>
  </body>
</html>