<!DOCTYPE html>
<html>
<body>
<head> <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">

  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
</head>
    
    <header>
    
        <nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo">Laboratorio de sesiones</a>

      </ul>
    </div>
  </nav>
    </header>
    <p></p>
    <br>
<form action="subir.php" method="post" enctype="multipart/form-data">
Selecciona una imagen:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload Image" name="submit">
</form>


<p></p>    
   <h2>Preguntas del laboratorio</h2> 
    <ul>
    <li>¿Por qué es importante hacer un session_unset() y luego un session_destroy()? <p>Son distintos, ya que unset eliminará una variable especifica del arreglo de sesión (por ejemplo el usuario), mientras que destroy eliminará toda la información de la sesión del usuario, para así desconectarse</p></li>
        
        <li>¿Cuál es la diferencia entre una variable de sesión y una cookie? <p>Pues que un cookie es un fragmento de información almacenado en el disco duro, esta info se almacena por petición del navegador. Las variables de sesión no se almacenan de tal modo.</p></li>
    
        <li>¿Qué técnicas se utilizan en sitios como facebook para que el usuario no sobreescriba sus fotos en el sistema de archivos cuando sube una foto con el mismo nombre?<p> Le asignan un nombre unico a las fotos, este está relacionado con la fecha y al parecer con el id del usuario.</p></li>
    </ul>
           
</body>
</html>