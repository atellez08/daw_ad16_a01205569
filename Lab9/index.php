/*Root User: atellez
//Database Name: c9

//Username: atellez
//https://lab8-a-atellez.c9users.io/phpmyadmin*/

<?php
    include("util.php");
    session_start();
    $f_nameErr = $emailErr = $passwordErr = $l_nameErr = "";
    $f_name = $email = $l_name = $password = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      if (empty($_POST["f_name"])) {
        $f_nameErr = "No escibiste tu nombre";
      } else {
        $f_name = $_POST["f_name"];
        //check if name only contains letters and whitespace
      }
      if (empty($_POST["l_name"])) {
        $l_nameErr = "No escibiste tu apellido";
      } else {
        $l_name = $_POST["l_name"];
        // check if name only contains letters and whitespace
        
      }
      
      if (empty($_POST["email"])) {
        $emailErr = "Email is required";
      } else {
        $email = $_POST["email"];
        // check if e-mail address is well-formed
        
      }
        
      if (empty($_POST["password"])) {
        $passwordErr = "No escribiste nada";
      } else {
        $password = $_POST["password"];
        // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
        
      }
      
      if(($f_nameErr === "") && ($emailErr === "") && ($passwordErr === "") && ($l_nameErr === "")){
          $_SESSION["username"] = $_POST["username"];
          include("accepted_view.html");
          
      } else {
          include("form_view.html");
      }
    } else {
        include("form_view.html");
    }
?>