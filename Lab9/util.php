<?php 

    function connect() {
        $mysql = mysqli_connect("localhost","atellez","","daw_labs");
        
        return $mysql;
    }

    function disconnect($mysql) {
        mysqli_close($mysql);
    }
    
    function nuevo_item($nombre, $descripcion, $cantidad) {
        
        $mysql = connect();
        // insert command specification 
        $query='INSERT INTO items (cantidad,nombre,descripcion) VALUES (?,?,?) ';
        // Preparing the statement 
        if (!($statement = $mysql->prepare($query))) {
            disconnect($mysql);
            die("Preparation failed: (" . $mysql->errno . ") " . $mysql->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("ssss", $nombre, $email, $nacimiento, $password)) {
            disconnect($mysql);
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
         // Executing the statement
         if (!$statement->execute()) {
            disconnect($mysql);
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
          }
          
         disconnect($mysql);
         return 1;
    }
    
    function items() {
        
        $mysql = connect();
        
        //Specification of the SQL query
        $query='SELECT * FROM items';
         // Query execution; returns identifier of the result group
        $results = $mysql->query($query);
        echo '<table><thead><tr><th>id</th><th>Nombre</th><th>Cantidad</th><th>Descripción</th></tr><tbody>';
         // cycle to explode every line of the results
        while ($row = mysqli_fetch_array($results, MYSQLI_BOTH)) {
                                        // Options: MYSQLI_NUM to use only numeric indexes
                                        // MYSQLI_ASSOC to use only name (string) indexes
                                        // MYSQLI_BOTH, to use both
            echo '<tr>';
            // use of numeric index
            echo '<td>'. $row[0].'</td>'; 
            // name of the column as associative index
        	echo '<td>'. $row['nombre'].'</td>'; 
        	echo '<td>'. $row['cantidad'].'</td>';
        	echo '<td>'. $row['descripcion'].'</td>';
        	echo '</tr>';
        }
        echo '</tbody></table>';
        // it releases the associated results
        mysqli_free_result($results);
        
        disconnect($mysql);
    }
    
    function login($mail, $password) {
        
        $mysql = connect();
        
        //Specification of the SQL query
        $query='SELECT * FROM clientes WHERE email="'.$mail.'" AND password="'.$password.'"';
         // Query execution; returns identifier of the result group
        $results = $mysql->query($query);
         // cycle to explode every line of the results
        if (mysqli_num_rows($results) > 0) {
               // it releases the associated results
                mysqli_free_result($results);
                disconnect($mysql);   
                return 1;
        } 
        // it releases the associated results
        mysqli_free_result($results);
        disconnect($mysql);
    }

?>