<?php
    include("modelo.php");
    
    session_start();
    
    if (isset($_SESSION["username"])) {
    
       $bd = connect();    
       include("dashboard_view.html");
    
    } else if (login($_POST["username"], $_POST["password"])) {
        
        $_SESSION["username"] = $_POST["username"];
        
        include("dashboard_view.html");
        
    } else {
        if (isset($_POST["username"] )) {
            $error = "Usuario y/o contraseña incorrectos ";
        }
        include("login_view.html");
    }
    
?>

