var hr = setInterval(reloj,1000);

function changeT1(){
	document.getElementById("content").style.color = "red";
	document.getElementById("content").style.fontFamily = "sans-serif";
}

function changeT2(){
	document.getElementById("content").style.color = "blue";
	document.getElementById("content").style.fontFamily = "serif";

}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}

function reloj(){
	var d = new Date();
    document.getElementById("fecha").innerHTML = "La hora es: " + d.toLocaleTimeString();
}
